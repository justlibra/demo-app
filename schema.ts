import {
    GraphQLSchema,
    GraphQLObjectType,
    GraphQLString,
    GraphQLInt,
    GraphQLList,
    GraphQLNonNull,
    GraphQLBoolean
} from 'graphql';
import * as AWS from "aws-sdk";

let dynamoDb = null;
const tableName = 'heroTable';

if (process.env.IS_OFFLINE === "true") {
  dynamoDb = new AWS.DynamoDB.DocumentClient({
    endpoint: process.env.DYNAMODB_ENDPOINT || "http://localhost:8000",
    region: "localhost"
  });
} else {
  dynamoDb = new AWS.DynamoDB.DocumentClient();
}

async function selectAllHero() {
  const params = {
    TableName: tableName
  };

  var abc = await dynamoDb.scan(params).promise().then(result => {
    return result;
  });
  return abc;
};

const heroInfo = new GraphQLObjectType({
    name: 'Hero',
    fields: {
        id: { type: new GraphQLNonNull(GraphQLString) },
        name: { type: new GraphQLNonNull(GraphQLString) }
    }
});

export const schemas = new GraphQLSchema({
    query: new GraphQLObjectType({
        name: 'Query',
        fields: {
            selectAllHero: {
                type: new GraphQLList(heroInfo),
                resolve: (parent, args) => selectAllHero()
            }
        }
    })
});