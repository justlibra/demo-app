import * as AWS from "aws-sdk";
import { ApolloServer, gql } from 'apollo-server-lambda';

let dynamoDb = null;
const tableName = 'heroTable';

if (process.env.IS_OFFLINE === "true") {
  dynamoDb = new AWS.DynamoDB.DocumentClient({
    endpoint: process.env.DYNAMODB_ENDPOINT || "http://localhost:8000",
    region: "localhost"
  });
} else {
  dynamoDb = new AWS.DynamoDB.DocumentClient();
}

const typeDefs = gql`
  type HeroInfo {
    id: Int,
    name: String
  }

  type Query {
    findHeroById(id: Int!): HeroInfo
    selectAllHero: [HeroInfo]
  }

  type Mutation {
    insertHero(id: Int!, name: String!): HeroInfo
    updateHeroById(id: Int!, name: String!): HeroInfo
    deleteHeroById(id: Int!): HeroInfo
  }
`;

// Provide resolver functions for your schema fields
const resolvers = {
  Query: {
    findHeroById: (_, event) => findHeroById(event),
    selectAllHero: () => selectAllHero()
  },
  Mutation: {
    insertHero: (_, event) => insertHero(event),
    updateHeroById: (_, event) => updateHeroById(event),
    deleteHeroById: (_, event) => deleteHeroById(event),
  },
};

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: ({ event, context }) => ({
    headers: event.headers,
    functionName: context.functionName,
    event,
    context,
  }),
});

export const graphqlHandler = server.createHandler();

async function selectAllHero() {
  const params = {
    TableName: tableName
  };

  return await dynamoDb.scan(params).promise().then(output => {
    return output.Items;
  });
};

async function findHeroById(event) {
  let id = event.id;
  const params = {
    Key: {
      id: id.toString()
    },
    TableName: tableName
  };

  return await dynamoDb.get(params).promise().then(output => {
    return output.Item;
  });
};

async function insertHero(event) {
  let { id, name } = event;
  const params = {
    Item: {
      id: id.toString(),
      name: name
    },
    TableName: tableName
  };
  return await dynamoDb.put(params).promise().then(() => {
    return params.Item;
  }).catch((err) => {
    throw err;
  });
}

async function updateHeroById(event) {
  let { id, name } = event;
  const params = {
    Key: {
      id: id.toString()
    },
    UpdateExpression: 'set #a = :name',
    ExpressionAttributeNames: { '#a': 'name' },
    ExpressionAttributeValues: {
      ":name": name,
    },
    TableName: tableName,
    ReturnValues: "ALL_NEW"
  };
  return await dynamoDb.update(params).promise().then((data) => {
    return data.Attributes;
  }).catch((err) => {
    throw err;
  });
}

async function deleteHeroById(event) {
  let { id } = event;
  const params = {
    Key: {
      id: id.toString()
    },
    TableName: tableName,
    ReturnValues: 'ALL_OLD'
  };
  return await dynamoDb.delete(params).promise().then((data) => {
    return data.Attributes;
  }).catch((err) => {
    throw err;
  });
}
