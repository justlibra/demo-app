var path = require('path');

module.exports = {
  entry: './handler.ts',
  target: 'node',
  stats: {warnings: false},
  module: {
    exprContextCritical: false
  },
  devtool: 'cheap-eval-source-map',
  resolve: {
    extensions: ['*', '.mjs', '.js', '.json', '.gql', '.graphql', '.ts', '.tsx', '.jsx', ]
  },
  output: {
    libraryTarget: 'commonjs',
    path: path.join(__dirname, 'dist'),
    filename: 'handler.js'
  },
  externals: {
    'aws-sdk': true
  },
};